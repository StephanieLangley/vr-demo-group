﻿using UnityEngine;
using System.Collections;

public class StarInfinte : MonoBehaviour {


    private Transform trans;
    private ParticleSystem.Particle[] starpoints;

    public int starsMax = 100;
    public float starSize = 1;
    public float starDistance = 10;
    public float starClipDistance = 4;
    private float starDistanceSqr;
    private float starClipDistanceSqr;


    // Use this for initialization
    void Start()
    {
        trans = transform;
        starDistanceSqr = starDistance * starDistance;
        starClipDistanceSqr = starClipDistance * starClipDistance;
    }


    private void CreateStars()
    {
        starpoints = new ParticleSystem.Particle[starsMax];

        for (int i = 0; i < starsMax; i++)
        {
            starpoints[i].position = Random.insideUnitSphere * starDistance + trans.position;
            starpoints[i].startColor = new Color(1, 1, 1, 1);
            starpoints[i].startSize = starSize;
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (starpoints == null) CreateStars();

         for (int i = 0; i < starsMax; i++)
         {

             if ((starpoints[i].position - trans.position).sqrMagnitude > starDistanceSqr)
             {
                 starpoints[i].position = Random.insideUnitSphere.normalized * starDistance + trans.position;
             }

             if ((starpoints[i].position - trans.position).sqrMagnitude <= starClipDistanceSqr)
             {
                 float percent = (starpoints[i].position - trans.position).sqrMagnitude / starClipDistanceSqr;
                 starpoints[i].startColor = new Color(1, 1, 1, percent);
                 starpoints[i].startSize = percent * starSize;
             }


         }

        GetComponent<ParticleSystem>().SetParticles(starpoints, starpoints.Length);

    }
}